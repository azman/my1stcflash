/*----------------------------------------------------------------------------*/
#include "my1stc_sync.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#ifndef __NO_USLEEP__
/* usleep */
#include <unistd.h>
/*----------------------------------------------------------------------------*/
#else
/* gettimeofday */
#include <sys/time.h>
void stc_wait_us(int usec) {
	struct timeval init, curr;
	int secs, chk1, chk2;
	gettimeofday(&init,0x0);
	secs = usec/1000000;
	if (secs>0) usec = usec%1000000;
	while (1) {
		gettimeofday(&curr,0x0);
		chk1 = curr.tv_sec - init.tv_sec;
		chk2 = curr.tv_usec - init.tv_usec;
		if (chk2<0) {
			chk1--;
			chk2 += 1000000;
		}
		if (chk1>=secs&&chk2>=usec)
			break;
	}
}
#endif
/*----------------------------------------------------------------------------*/
int stc_sync_exec(stc_sync_t* sync, int code, int tout) {
	int wait;
	wait = 0;
	do {
		uart_send_byte(sync->port,code);
		stc_wait_us(sync->wait);
		if (tout>0) {
			wait += sync->wait;
			if (wait>tout) return STC_SYNC_MISS;
		}
		if (get_keyhit()==KEY_ESCAPE)
			return STC_SYNC_NONE;
	} while (!uart_incoming(sync->port));
	return STC_SYNC_DONE;
}
/*----------------------------------------------------------------------------*/
void stc_sync_init(stc_sync_t* sync,my1uart_t* port) {
	sync->stat = 0;
	sync->wait = STC_SYNC_WAIT_US;
	sync->port = port;
}
/*----------------------------------------------------------------------------*/
