/*----------------------------------------------------------------------------*/
#ifndef __MY1STC_PACK_H__
#define __MY1STC_PACK_H__
/*----------------------------------------------------------------------------*/
#include "my1stc_sync.h"
#include "my1bytes.h"
#include "my1stc_db.h"
/*----------------------------------------------------------------------------*/
#define STC_PACK_FLAG_DEBUG 0x01
/*----------------------------------------------------------------------------*/
#define STC_PACK_INITSIZE 256
/* packet size will never be this! */
#define STC_PACK_ABORT 0x10000
#define STC_PACK_WAIT_US 500000
/*----------------------------------------------------------------------------*/
#define STC_PACK_M0 0x46
#define STC_PACK_M1 0xb9
#define STC_PACK_ME 0x16
#define STC_PACK_HOST2MCU 0x6a
#define STC_PACK_MCU2HOST 0x68
/*----------------------------------------------------------------------------*/
#define STC_PACK_OK 0
#define STC_PACK_ERROR UINT32_MSB1
#define STC_PACK_ERROR_MASK    (STC_PACK_ERROR|0x000F)
#define STC_PACK_TIMEOUT_ERROR (STC_PACK_ERROR|0x0001)
#define STC_PACK_M0_ERROR      (STC_PACK_ERROR|0x0002)
#define STC_PACK_M1_ERROR      (STC_PACK_ERROR|0x0003)
#define STC_PACK_DIR_ERROR     (STC_PACK_ERROR|0x0004)
#define STC_PACK_ME_ERROR      (STC_PACK_ERROR|0x0005)
#define STC_PACK_CHKSUM_ERROR  (STC_PACK_ERROR|0x0006)
/*----------------------------------------------------------------------------*/
typedef struct _stc_pack_t {
	stc_sync_t sync;
	my1bytes_t rbuf, tbuf;
	byte08_t chk0, chk1;
	byte08_t chkD, chkZ;
	word16_t size, csum;
	int wait, temp;
	unsigned int flag; /* options flag */
	unsigned int step; /* counter for stc_pack_wait */
} stc_pack_t;
/*----------------------------------------------------------------------------*/
void stc_pack_init(stc_pack_t*,my1uart_t*);
void stc_pack_free(stc_pack_t*);
int stc_pack_wait(stc_pack_t*);
int stc_pack_send(stc_pack_t*);
int stc_pack_sync(stc_pack_t*);
/*----------------------------------------------------------------------------*/
#endif /** __MY1STC_PACK_H__ */
/*----------------------------------------------------------------------------*/
