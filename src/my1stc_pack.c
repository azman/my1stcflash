/*----------------------------------------------------------------------------*/
#include "my1stc_pack.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void stc_pack_init(stc_pack_t* pack, my1uart_t* port) {
	stc_sync_init(&pack->sync,port);
	bytes_init(&pack->rbuf);
	bytes_make(&pack->rbuf,STC_PACK_INITSIZE);
	bytes_init(&pack->tbuf);
	bytes_make(&pack->tbuf,STC_PACK_INITSIZE);
	pack->wait = STC_PACK_WAIT_US;
	pack->flag = 0;
}
/*----------------------------------------------------------------------------*/
void stc_pack_free(stc_pack_t* pack) {
	bytes_free(&pack->rbuf);
	bytes_free(&pack->tbuf);
}
/*----------------------------------------------------------------------------*/
int stc_pack_wait(stc_pack_t* pack) {
	int loop;
	word16_t csum;
	pack->sync.stat &= ~STC_PACK_ERROR_MASK;
	pack->step = 0;
	bytes_null(&pack->rbuf);
	while (1) {
		if (uart_incoming(pack->sync.port))
			break;
		if (get_keyhit()==KEY_ESCAPE)
			return STC_PACK_ABORT;
	}
	/* start reading - get start markers */
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->chk0 = pack->temp;
	pack->step++;
	if (pack->chk0!=STC_PACK_M0) {
		pack->sync.stat |= STC_PACK_M0_ERROR;
		return pack->sync.stat;
	}
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->chk1 = pack->temp;
	pack->step++;
	if (pack->chk1!=STC_PACK_M1) {
		pack->sync.stat |= STC_PACK_M1_ERROR;
		return pack->sync.stat;
	}
	/* get packet direction flag */
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->chkD = pack->temp;
	pack->step++;
	if (pack->chkD!=STC_PACK_MCU2HOST) {
		pack->sync.stat |= STC_PACK_DIR_ERROR;
		return pack->sync.stat;
	}
	csum = pack->chkD;
	/* get size */
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->size = (pack->temp&0xff)<<8;
	pack->step++;
	csum += (word16_t)pack->temp;
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->size |= pack->temp&0xff;
	pack->step++;
	csum += (word16_t)pack->temp;
	/* get data packet */
	for (loop=0;loop<pack->size-6;loop++) {
		pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
		if (pack->temp==UART_TIMEOUT) {
			pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
			return pack->sync.stat;
		}
		bytes_slip(&pack->rbuf,(byte08_t)pack->temp);
		pack->step++;
		csum += (word16_t)pack->temp;
	}
	/* get checksum */
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->csum = (pack->temp&0xff)<<8;
	pack->step++;
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->csum |= pack->temp&0xff;
	pack->step++;
	if (pack->csum!=csum) {
		pack->sync.stat |= STC_PACK_CHKSUM_ERROR;
		return pack->sync.stat;
	}
	/* check end marker */
	pack->temp = uart_read_byte_timed(pack->sync.port,pack->wait);
	if (pack->temp==UART_TIMEOUT) {
		pack->sync.stat |= STC_PACK_TIMEOUT_ERROR;
		return pack->sync.stat;
	}
	pack->chkZ = pack->temp;
	pack->step++;
	if (pack->chkZ!=STC_PACK_ME) {
		pack->sync.stat |= STC_PACK_ME_ERROR;
		return pack->sync.stat;
	}
	if (pack->flag&STC_PACK_FLAG_DEBUG) {
		printf("\n<- Packet data:");
		printf(" %02X",pack->chk0);
		printf(" %02X",pack->chk1);
		printf(" %02X",pack->chkD);
		printf(" %02X",(pack->size>>8)&0xff);
		printf(" %02X",pack->size&0xff);
		for (loop=0;loop<pack->rbuf.fill;loop++)
			printf(" %02X",pack->rbuf.data[loop]);
		printf(" %02X",(pack->csum>>8)&0xff);
		printf(" %02X",pack->csum&0xff);
		printf(" %02X\n",pack->chkZ);
	}
	return STC_PACK_OK;
}
/*----------------------------------------------------------------------------*/
int stc_pack_send(stc_pack_t* pack) {
	int loop;
/**
	pack->chk0 = STC_PACK_M0;
	pack->chk1 = STC_PACK_M1;
	pack->chkD = STC_PACK_HOST2MCU;
	pack->chkZ = STC_PACK_ME;
**/
	pack->size = pack->tbuf.fill + 6;
	pack->csum = STC_PACK_HOST2MCU;
	pack->csum += (pack->size>>8)&0xff;
	pack->csum += pack->size&0xff;
	for (loop=0;loop<pack->tbuf.fill;loop++)
		pack->csum += pack->tbuf.data[loop]&0xff;
	/* send the packet */
	uart_send_byte(pack->sync.port,STC_PACK_M0);
	uart_send_byte(pack->sync.port,STC_PACK_M1);
	uart_send_byte(pack->sync.port,STC_PACK_HOST2MCU);
	uart_send_byte(pack->sync.port,(pack->size>>8)&0xff);
	uart_send_byte(pack->sync.port,pack->size&0xff);
	for (loop=0;loop<pack->tbuf.fill;loop++)
		uart_send_byte(pack->sync.port,pack->tbuf.data[loop]);
	uart_send_byte(pack->sync.port,(pack->csum>>8)&0xff);
	uart_send_byte(pack->sync.port,pack->csum&0xff);
	uart_send_byte(pack->sync.port,STC_PACK_ME);
	if (pack->flag&STC_PACK_FLAG_DEBUG) {
		printf("-> Packet data:");
		printf(" %02X",STC_PACK_M0);
		printf(" %02X",STC_PACK_M1);
		printf(" %02X",STC_PACK_HOST2MCU);
		printf(" %02X",(pack->size>>8)&0xff);
		printf(" %02X",pack->size&0xff);
		for (loop=0;loop<pack->tbuf.fill;loop++)
			printf(" %02X",pack->tbuf.data[loop]);
		printf(" %02X",(pack->csum>>8)&0xff);
		printf(" %02X",pack->csum&0xff);
		printf(" %02X\n",pack->chkZ);
	}
	return STC_PACK_OK;
}
/*----------------------------------------------------------------------------*/
int stc_pack_sync(stc_pack_t* pack) {
	int stat, test;
	makeraw_iterminal();
	do {
		stat = stc_sync_exec(&pack->sync,STC_SYNC_CHAR,0);
		/* *DONE ot *NONE - no timeout specified */
		if (stat==STC_SYNC_DONE) {
			test = stc_pack_wait(pack);
			if (test==STC_PACK_ABORT) stat = STC_SYNC_NONE;
			/* give this another chance! */
			if (test!=STC_PACK_OK) stat = STC_SYNC_MISS;
		}
	} while (stat==STC_SYNC_MISS);
	restore_iterminal();
	return stat;
}
/*----------------------------------------------------------------------------*/
