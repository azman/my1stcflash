/*----------------------------------------------------------------------------*/
#ifndef __MY1STC_STC15W_H__
#define __MY1STC_STC15W_H__
/*----------------------------------------------------------------------------*/
#include "my1stc_core.h"
/* reuse functions for stc12 */
#include "my1stc_stc12.h"
/*----------------------------------------------------------------------------*/
/* stcgal uses 14, but gets up until 38! */
#define STC15_PACK_MINSIZE 38
#define STC15_SHAKE_SYNC_US 1000000
#define STC15_SHAKE_WAIT_US 100000
/*----------------------------------------------------------------------------*/
#define STC15_CODE_BLOCKSIZE 64
#define STC15_ERASE_COUNTDOWN 0x5e
/*----------------------------------------------------------------------------*/
#define STC15_EXTCLK_ERROR      (STC_CORE_ERROR|0x0100)
#define STC15_HANDSHAKE1_ERROR  (STC_CORE_ERROR|0x0200)
#define STC15_SHAKEMAGIC1_ERROR (STC_CORE_ERROR|0x0300)
#define STC15_SHAKERANGE1_ERROR (STC_CORE_ERROR|0x0400)
#define STC15_SHAKERANGE2_ERROR (STC_CORE_ERROR|0x0500)
#define STC15_HANDSHAKE2_ERROR  (STC_CORE_ERROR|0x0600)
#define STC15_SHAKEMAGIC2_ERROR (STC_CORE_ERROR|0x0700)
#define STC15_SHAKETRIM1_ERROR  (STC_CORE_ERROR|0x0800)
#define STC15_SHAKETRIM2_ERROR  (STC_CORE_ERROR|0x0900)
#define STC15_HANDSHAKE3_ERROR  (STC_CORE_ERROR|0x0A00)
#define STC15_SHAKEMAGIC3_ERROR (STC_CORE_ERROR|0x0B00)
#define STC15_HANDSHAKE_ERROR   (STC_CORE_ERROR|0x0C00)
#define STC15_SHAKEMAGIC_ERROR  (STC_CORE_ERROR|0x0D00)
#define STC15_ERASEMEM_ERROR    (STC_CORE_ERROR|0x0E00)
#define STC15_FLASHMEM_ERROR    (STC_CORE_ERROR|0x0F00)
#define STC15_SENDOPTS_ERROR    (STC_CORE_ERROR|0x1000)
/*----------------------------------------------------------------------------*/
int stc15_initstuff(stc_core_t* core) {
	/* recalc/detect freq */
	if (!(core->pdat[7]&0x01)) {
		core->xcnt = (int)core->pdat[13]<<8 | core->pdat[14];
		core->fmcu = core->hand*core->xcnt;
	}
	else {
		core->xcnt = 0;
		core->fmcu = core->pdat[8]<<24;
		core->fmcu += core->pdat[9]<<16;
		core->fmcu += core->pdat[10]<<8;
		core->fmcu += core->pdat[11];
		if (core->fmcu==-1) core->fmcu = 0;
	}
	/* get bsl firmware version */
	core->bslv[0] = (core->pdat[17]>>4)+0x30;
	core->bslv[1] = '.';
	core->bslv[2] = (core->pdat[17]&0x0f)+0x30;
	core->bslv[3] = '.';
	core->bslv[4] = (core->pdat[22]&0x0f)+0x30;
	core->bslv[5] = core->pdat[18];
	core->bslv[6] = 0x0;
	/** trim adjust for 24 MHz? used or extclk! */
	// fcount24 = core->pdat[4];
	/* do we need this??? */
	// wake_freq_hz = (int)core->pdat[1]<<8 | core->pdat[2];
	/* get options */
	if (core->dcnt<STC15_PACK_MINSIZE) {
		CORESTAT(core) |= STC_CORE_STAT_ERROR;
	}
	else {
		/* should have at least 4 bytes msr[0]-msr[3] <= 5,6,7,12,37 */
		bytes_slip(&core->opts,core->pdat[5]);
		bytes_slip(&core->opts,core->pdat[6]);
		bytes_slip(&core->opts,core->pdat[7]);
		bytes_slip(&core->opts,core->pdat[12]);
		bytes_slip(&core->opts,core->pdat[37]);
	}
	/* not supporting this for now */
	if (core->xcnt)
		CORESTAT(core) |= STC15_EXTCLK_ERROR;
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc15_select_range(unsigned char *tx, int tcnt,
		unsigned char *rx, int rcnt, int ctgt, int *trim, int* trng) {
	unsigned char *pcal, *pcha, *pchk;
	int loop, clen, cntA, cntB, trmA, trmB, trmR;
	int trim_tgt;
	float tmp1, tmp2;
	pcha = &tx[2];
	pcal = &rx[2];
	clen = (int) rx[1];
	if ((rcnt-2)<2*clen) return -1;
	for (loop=0;loop<(clen-1);loop++) {
		cntA = (int)pcal[2*loop]<<8 | pcal[2*loop+1];
		cntB = (int)pcal[2*loop+2]<<8 | pcal[2*loop+3];
		pchk = &pcha[2*loop];
		trmA = pchk[0];
		trmB = pchk[2]; /* skip 1 */
		trmR = pchk[3];
		if (((cntA<=ctgt)&&(cntB>=ctgt))||((cntB<=ctgt)&&(cntA>=ctgt))) {
			tmp1 = (float)(trmB-trmA)/(cntB-cntA);
			tmp2 = (float)trmA - (tmp1*cntA);
			trim_tgt = (int)(tmp1*ctgt+tmp2);
			if (trim_tgt<0||trim_tgt>0x10000)
				return -1;
			*trim = trim_tgt;
			*trng = trmR;
			return 0;
		}
	}
	return -1;
}
/*----------------------------------------------------------------------------*/
int stc15_select_trim(unsigned char *tx, int tcnt,
		unsigned char *rx, int rcnt, int ctgt, int *best, int *xcnt) {
	unsigned char *pcal, *pcha, *pchk;
	int loop, clen, cntA, trmA, trmR, bcnt, bchk, diff;
	pcha = &tx[2];
	pcal = &rx[2];
	clen = (int) rx[1];
	if ((rcnt-2)<2*clen) return -1;
	bcnt = 0x7fffffff; bchk = -1;
	for (loop=0;loop<clen;loop++) {
		cntA = (int)pcal[2*loop]<<8|pcal[2*loop+1];
		pchk = &pcha[2*loop];
		trmA = pchk[0];
		trmR = pchk[1];
		diff = cntA - ctgt;
		if (diff<0) diff = -diff;
		if (diff<bcnt) {
			bcnt = diff;
			bchk = 0;
			*best = ((trmR&0xff)<<8)|(trmA&0xff);
			*xcnt = cntA;
		}
	}
	return bchk;
}
/*----------------------------------------------------------------------------*/
int stc15_handshake(stc_core_t* core) {
	int ucnt, pcnt, test, loop;
	int trmU, rngU, trmP, rngP;
	float user_freq, prog_freq;
	unsigned char pack[] = { 0x00, 0x0c,
		0x00, 0xc0, 0x80, 0xc0, 0xff, 0xc0,
		0x00, 0x80, 0x80, 0x80, 0xff, 0x80,
		0x00, 0x40, 0x80, 0x40, 0xff, 0x40,
		0x00, 0x00, 0x80, 0x00, 0xc0, 0x00 }; /** 28 bytes? */
	/*user_freq = trim_freq*/
	/*if (user_freq<=0.0) user_freq = core->fmcu */
	user_freq = (float)core->fmcu; /* trim IS 0! */
	prog_freq = 22118400.0; /* where do we get this?  */
	ucnt = round(user_freq / (core->hand/2));
	pcnt = round(prog_freq / (core->hand/2));
	/* round 1 */
	bytes_null(&core->pack.tbuf);
	bytes_more(&core->pack.tbuf,pack,sizeof(pack));
	stc_pack_send(&core->pack);
	test = stc_sync_exec(&core->pack.sync,0xfe,STC15_SHAKE_SYNC_US);
	if (test!=STC_SYNC_DONE) {
		CORESTAT(core) |= STC_PACK_TIMEOUT_ERROR;
		return CORESTAT(core);
	}
	test = stc_pack_wait(&core->pack);
	if (test!=STC_PACK_OK) {
		CORESTAT(core) |= STC15_HANDSHAKE1_ERROR;
		return CORESTAT(core);
	}
	if (core->dcnt<2||core->pdat[0]!=0x00) {
		CORESTAT(core) |= STC15_SHAKEMAGIC1_ERROR;
		return CORESTAT(core);
	}
	if (stc15_select_range(pack,sizeof(pack),
			core->pdat,core->dcnt,ucnt,&trmU,&rngU)) {
		CORESTAT(core) |= STC15_SHAKERANGE1_ERROR;
		return CORESTAT(core);
	}
	if (stc15_select_range(pack,sizeof(pack),
			core->pdat,core->dcnt,pcnt,&trmP,&rngP)) {
		CORESTAT(core) |= STC15_SHAKERANGE2_ERROR;
		return CORESTAT(core);
	}
	/* round 2 */
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x0c);
	for (loop=trmU-3;loop<(trmU+3);loop++) {
		bytes_slip(&core->pack.tbuf,loop&0xff);
		bytes_slip(&core->pack.tbuf,rngU);
	}
	for (loop=trmP-3;loop<(trmP+3);loop++) {
		bytes_slip(&core->pack.tbuf,loop&0xff);
		bytes_slip(&core->pack.tbuf,rngP);
	}
	stc_pack_send(&core->pack);
	test = stc_sync_exec(&core->pack.sync,0xfe,STC15_SHAKE_SYNC_US);
	if (test!=STC_SYNC_DONE) {
		CORESTAT(core) |= STC_PACK_TIMEOUT_ERROR;
		return CORESTAT(core);
	}
	test = stc_pack_wait(&core->pack);
	if (test!=STC_PACK_OK) {
		CORESTAT(core) |= STC15_HANDSHAKE2_ERROR;
		return CORESTAT(core);
	}
	if (core->dcnt<2||core->pdat[0]!=0x00) {
		CORESTAT(core) |= STC15_SHAKEMAGIC2_ERROR;
		return CORESTAT(core);
	}
	if (stc15_select_trim(core->pack.tbuf.data,core->pack.tbuf.fill,
			core->pdat,core->dcnt,ucnt,&trmU,&rngU)) {
		CORESTAT(core) |= STC15_SHAKETRIM1_ERROR;
		return CORESTAT(core);
	}
	if (stc15_select_trim(core->pack.tbuf.data,core->pack.tbuf.fill,
			core->pdat,core->dcnt,pcnt,&trmP,&rngP)) {
		CORESTAT(core) |= STC15_SHAKETRIM2_ERROR;
		return CORESTAT(core);
	}
	/* trim freq - used in opts */
	core->trmv = trmU;
	core->trmf = (int)round((float)rngU * ((float)core->hand/2));
	/* switch baudrate */
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x01);
	bytes_slip(&core->pack.tbuf,trmP&0xff);
	bytes_slip(&core->pack.tbuf,(trmP>>8)&0xff);
	loop = 0x10000 - (int)((float)prog_freq/(core->baud*4));
	bytes_slip(&core->pack.tbuf,(loop>>8)&0xff);
	bytes_slip(&core->pack.tbuf,loop&0xff);
	bytes_slip(&core->pack.tbuf,(trmU>>8)&0xff);
	bytes_slip(&core->pack.tbuf,trmU&0xff);
	loop = stc12_iapw_value(prog_freq);
	bytes_slip(&core->pack.tbuf,loop&0xff);
	stc_pack_send(&core->pack);
	test = stc_sync_exec(&core->pack.sync,0xfe,STC15_SHAKE_SYNC_US);
	if (test!=STC_SYNC_DONE) {
		CORESTAT(core) |= STC_PACK_TIMEOUT_ERROR;
		return CORESTAT(core);
	}
	test = stc_pack_wait(&core->pack);
	if (test!=STC_PACK_OK) {
		CORESTAT(core) |= STC15_HANDSHAKE3_ERROR;
		return CORESTAT(core);
	}
	if (core->dcnt<1||core->pdat[0]!=0x01) {
		CORESTAT(core) |= STC15_SHAKEMAGIC3_ERROR;
		return CORESTAT(core);
	}
	stc_wait_us(STC15_SHAKE_WAIT_US);
	stc_core_baud(core,core->baud);
	/* prepare new baudrate */
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x05);
	/* only if bsl >= 7.2? */
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x5a);
	bytes_slip(&core->pack.tbuf,0xa5);
	stc_pack_send(&core->pack);
	test = stc_pack_wait(&core->pack);
	if (test!=STC_PACK_OK) {
		CORESTAT(core) |= STC15_HANDSHAKE_ERROR;
		return CORESTAT(core);
	}
	if (core->dcnt<1||core->pdat[0]!=0x05) {
		CORESTAT(core) |= STC15_SHAKEMAGIC_ERROR;
		return CORESTAT(core);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc15_erasemem(stc_core_t* core) {
	int loop;
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x03); /* erase command */
	bytes_slip(&core->pack.tbuf,0x00); /* flash only */
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x5a);
	bytes_slip(&core->pack.tbuf,0xa5);
	stc_pack_send(&core->pack);
	if (stc_pack_wait(&core->pack)!=STC_PACK_OK||
			core->dcnt<1||core->pdat[0]!=0x03) {
		CORESTAT(core) |= STC15_ERASEMEM_ERROR;
		return CORESTAT(core);
	}
	if (core->dcnt>=8) {
		core->xuid[0] = 0xff;
		for (loop=1;loop<8;loop++)
			core->xuid[loop] = core->pdat[loop];
	}
	else core->xuid[0] = 0x00;
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc15_flashmem(stc_core_t* core) {
	int size, loop, iter, test;
	/* send in 'packets' */
	for (size=0,loop=0;size<core->code.fill;size+=STC15_CODE_BLOCKSIZE) {
		bytes_null(&core->pack.tbuf);
		bytes_slip(&core->pack.tbuf,size?0x02:0x22);
		bytes_slip(&core->pack.tbuf,(size>>8)&0xff);
		bytes_slip(&core->pack.tbuf,size&0xff);
		bytes_slip(&core->pack.tbuf,0x5a);
		bytes_slip(&core->pack.tbuf,0xa5);
		/* copy data */
		for (iter=0;iter<STC15_CODE_BLOCKSIZE;iter++) {
			if (loop<core->code.fill)
				bytes_slip(&core->pack.tbuf,core->code.data[loop++]);
			else
				bytes_slip(&core->pack.tbuf,0x0); /* zero-pad */
		}
		stc_pack_send(&core->pack);
		test = stc_pack_wait(&core->pack);
		if (test!=STC_PACK_OK) {
			CORESTAT(core) |= STC15_FLASHMEM_ERROR;
			return CORESTAT(core);
		}
		if (core->dcnt<2||core->pdat[0]!=0x02||core->pdat[1]!=0x54) {
			CORESTAT(core) |= STC15_FLASHMEM_ERROR;
			return CORESTAT(core);
		}
	}
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x07); /* finish up */
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x5a);
	bytes_slip(&core->pack.tbuf,0xa5);
	stc_pack_send(&core->pack);
	test = stc_pack_wait(&core->pack);
	if (test!=STC_PACK_OK) {
		CORESTAT(core) |= STC15_FLASHMEM_ERROR;
		return CORESTAT(core);
	}
	if (core->dcnt<2||core->pdat[0]!=0x07||core->pdat[1]!=0x54) {
		CORESTAT(core) |= STC15_FLASHMEM_ERROR;
		return CORESTAT(core);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc15_sendopts(stc_core_t* core) {
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x04);
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x5a);
	bytes_slip(&core->pack.tbuf,0xa5);
	//options?
	bytes_fill_size(&core->pack.tbuf,0xff,23);
	bytes_slip(&core->pack.tbuf,(core->trmf>>24)&0xff);
	bytes_slip(&core->pack.tbuf,0xff);
	bytes_slip(&core->pack.tbuf,(core->trmf>>16)&0xff);
	bytes_slip(&core->pack.tbuf,0xff);
	bytes_slip(&core->pack.tbuf,(core->trmf>>8)&0xff);
	bytes_slip(&core->pack.tbuf,0xff);
	bytes_slip(&core->pack.tbuf,core->trmf&0xff);
	bytes_slip(&core->pack.tbuf,0xff);
	bytes_slip(&core->pack.tbuf,core->opts.data[3]);
	bytes_fill_size(&core->pack.tbuf,0xff,23);
	/* TODO: merge these? */
	if (core->opts.fill>4)
		bytes_slip(&core->pack.tbuf,core->opts.data[4]);
	else
		bytes_slip(&core->pack.tbuf,0xff);
	bytes_fill_size(&core->pack.tbuf,0xff,3);
	bytes_slip(&core->pack.tbuf,core->trmv&0xff);
	bytes_slip(&core->pack.tbuf,((core->trmv>>8)&0xff)+0x3f);
	bytes_more(&core->pack.tbuf,core->opts.data,4);
	stc_pack_send(&core->pack);
	/* stcgal only checks for 0x54 - i got 0x46 @ index 1! */
	if (stc_pack_wait(&core->pack)!=STC_PACK_OK||core->dcnt<2||
			core->pdat[0]!=0x04||core->pdat[1]!=0x54) {
		CORESTAT(core) |= STC15_SENDOPTS_ERROR;
		return CORESTAT(core);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1STC_STC15W_H__ */
/*----------------------------------------------------------------------------*/
