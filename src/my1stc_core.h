/*----------------------------------------------------------------------------*/
#ifndef __MY1STC_CORE_H__
#define __MY1STC_CORE_H__
/*----------------------------------------------------------------------------*/
#include "my1stc_pack.h"
#include "my1uart.h"
#include "my1stc_db.h"
/*----------------------------------------------------------------------------*/
#define STC_SYNC_FAIL STC_SYNC_INIT
#define STC_PACK_STAT_SIZE 23
/*----------------------------------------------------------------------------*/
#define STC_CORE_ERROR STC_PACK_ERROR
#define STC_CORE_ERROR_MASK 0x00F0
#define STC_CORE_FIND_ERROR (STC_CORE_ERROR|0x0010)
#define STC_CORE_PORT_ERROR (STC_CORE_ERROR|0x0020)
#define STC_CORE_BAUD_ERROR (STC_CORE_ERROR|0x0030)
#define STC_CORE_OPEN_ERROR (STC_CORE_ERROR|0x0040)
#define STC_CORE_STAT_ERROR (STC_CORE_ERROR|0x0050)
/*----------------------------------------------------------------------------*/
struct _stc_core_t; /* forward declaration */
typedef int (*stc_task_t)(struct _stc_core_t*);
/*----------------------------------------------------------------------------*/
typedef struct _stc_core_t {
	stc_pack_t pack;
	my1bytes_t code, opts;
	my1uart_t port;
	int baud, hand; /* transfer/handshake baud rates */
	/* info for data from received packet - alias for pack.rbuf  */
	byte08_t *pdat;
	int dcnt;
	/* info for detected device */
	stcmcu_t *pdev;
	int uuid; /* mcu magic/unique id */
	int fmcu; /* mcu freq in hz */
	int xcnt; /* external clk stuff (stc15w?)*/
	int trmv, trmf; /* stc15w */
	char bslv[7]; /* bsl version */
	/* uid? 7 bytes? 1st byte as found flag 0xff */
	unsigned char xuid[8];
	/* erase param - for debugging */
	int emx0, emx1;
	/* main tasks */
	stc_task_t do_handshake;
	stc_task_t do_erase_mem;
	stc_task_t do_flash_mem;
	stc_task_t do_send_opts;
	stc_task_t do_reset_dev;
} stc_core_t;
/*----------------------------------------------------------------------------*/
#define COREPACK(core) ((stc_core_t*)core)->pack
#define CORESYNC(core) COREPACK(core).sync
#define CORESTAT(core) CORESYNC(core).stat
/*----------------------------------------------------------------------------*/
void stc_core_init(stc_core_t*);
void stc_core_free(stc_core_t*);
int stc_core_prep(stc_core_t*);
int stc_core_sync(stc_core_t*);
int stc_core_baud(stc_core_t*,int);
/*----------------------------------------------------------------------------*/
#endif /** __MY1STC_CORE_H__ */
/*----------------------------------------------------------------------------*/
