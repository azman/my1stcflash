/*----------------------------------------------------------------------------*/
#ifndef __MY1STC_DBH__
#define __MY1STC_DBH__
/*----------------------------------------------------------------------------*/
#define STCMCU_FLAG_ACTIVE 0x0001
#define STCMCU_FLAG_STC12 0x0100
#define STCMCU_FLAG_STC15 0x0200
/*----------------------------------------------------------------------------*/
#define STCMCU_STC12C5A60S2 (STCMCU_FLAG_STC12|STCMCU_FLAG_ACTIVE)
#define STCMCU_STC15W204S (STCMCU_FLAG_STC15|STCMCU_FLAG_ACTIVE)
/*----------------------------------------------------------------------------*/
#define STC_DEVICE_NAME_LEN 16
/*----------------------------------------------------------------------------*/
/**
 * mcu-uuid (mcu-id1|mcu-id2), mcu-flag,
 * mcu-flash-size (kB), mcu-eeprom-size (kB), mcu-name
**/
typedef struct _stcmcu_t {
	int uuid, flag, fmsz, emsz;
	char name[STC_DEVICE_NAME_LEN];
} stcmcu_t;
/*----------------------------------------------------------------------------*/
stcmcu_t* stc_db_find(int);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
