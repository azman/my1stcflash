/*----------------------------------------------------------------------------*/
#include "my1stc_core.h"
#include "my1linehex.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define PROGNAME "my1stcsync"
#ifndef PROGVERS
#define PROGVERS "build"
#endif
/*----------------------------------------------------------------------------*/
void show_packet(stc_core_t* core) {
	unsigned char *pdat;
	int size, loop, temp;
	pdat = core->pack.rbuf.data;
	size = core->pack.step;
	printf("---------------\n");
	printf("PACKET:%s (Size:%d)\n",size<3?"???":
		core->pack.chkD==STC_PACK_MCU2HOST?"mcu2host":
		core->pack.chkD==STC_PACK_HOST2MCU?"host2mcu":"UNKNOWN!",size);
	printf("---------------\n");
	if (size>0) printf("%02X (%02X) ",core->pack.chk0,STC_PACK_M0);
	if (size>1) printf("%02X (%02X) ",core->pack.chk1,STC_PACK_M1);
	if (size>2) printf("%02X (type) ",core->pack.chkD);
	if (size>3) printf("%02X ",(core->pack.size>>8)&0xff);
	if (size>4) printf("%02X ",core->pack.size&0xff);
	for (loop=0;loop<core->pack.rbuf.fill;loop++) {
		printf("%02X ",pdat[loop]);
		if (!loop) printf(" (flag) ");
	}
	temp = size - core->pack.rbuf.fill;
	if (temp>0) printf("%02X ",(core->pack.csum>>8)&0xff);
	if (temp>1) printf("%02X ",core->pack.csum&0xff);
	if (temp>2) printf("%02X (%02X) ",core->pack.chkZ,STC_PACK_ME);
	if (size>0) printf("\n");
	if (core->pack.rbuf.fill>=22)
		printf("@@FCHK:[%02X] UUID:%02X%02X\n",pdat[0],pdat[20],pdat[21]);
}
/*----------------------------------------------------------------------------*/
void show_core_baud(stc_core_t* core) {
	my1uart_conf_t conf;
	uart_get_config(&core->port,&conf);
	printf("@@ Core baudrate: %d\n",uart_actual_baudrate(conf.baud));
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	stc_core_t stcc;
	char *pcod, *pchk;
	int test,loop;
	printf("\n%s - STC Sync Tool (version %s)\n",PROGNAME,PROGVERS);
	printf("  => by azman@my1matrix.org\n\n");
	printf("-- Core init... ");
	stc_core_init(&stcc);
	printf("done.\n");
	do {
		pcod = 0x0;
		for (loop=1;loop<argc;loop++) {
			if (argv[loop][0]=='-') {
				if (argv[loop][1]=='-') test = 2; else test = 1;
				pchk = &argv[loop][test];
			}
			else {
				/* check if this is a readable file */
				FILE* test;
				test = fopen(argv[loop],"r");
				if (test) {
					fclose(test);
					pcod = argv[loop];
				}
				else {
					printf("** Unknown param '%s'!\n",argv[loop]);
					CORESTAT(&stcc) |= STC_CORE_ERROR;
				}
				continue;
			}
			if (!strncmp(pchk,"baud",5)) {
				if (get_param_int(argc,argv,&loop,&test)<0) {
					printf("** Cannot get transfer baud rate!\n");
					CORESTAT(&stcc) |= STC_CORE_ERROR;
				}
				else if (uart_encoded_baudrate(test)<0) {
					printf("** Invalid transfer baud rate! (%d)\n",test);
					CORESTAT(&stcc) |= STC_CORE_ERROR;
				}
				else stcc.baud = test;
			}
			else if (!strncmp(pchk,"hand",5)) {
				if (get_param_int(argc,argv,&loop,&test)<0) {
					printf("** Cannot get handshake baud rate!\n");
					CORESTAT(&stcc) |= STC_CORE_ERROR;
				}
				else if (uart_encoded_baudrate(test)<0) {
					printf("** Invalid handshake baud rate! (%d)\n",test);
					CORESTAT(&stcc) |= STC_CORE_ERROR;
				}
				else stcc.hand = test;
			}
			else {
				printf("** Invalid option '%s'!\n",argv[loop]);
				CORESTAT(&stcc) |= STC_CORE_ERROR;
			}
		}
		if (pcod) {
			printf("-- Loading code HEX file... ");
			test = bytes_load_ihex(&stcc.code, pcod);
			if (test<0) {
				printf("fail! (%d)\n",test);
				break;
			}
			printf("done! (%d/%d)\n",stcc.code.fill,stcc.code.size);
			/* align to block size? */
			test = stcc.code.size%512;
			if (test) {
				bytes_make(&stcc.code,stcc.code.size+(512-test));
				bytes_fill(&stcc.code,0xff);
#ifdef MY1DEBUG0
				printf("@@ Aligned to block size (%d|%d)[%d].\n",
					stcc.code.fill,stcc.code.size,stcc.code.size%512);
#endif
			}
/*
			printf("@@ Code:");
			for (loop=0;loop<code.fill;loop++) {
				if (loop%16==0)
					printf("\n   %04x:",loop);
				printf(" %02x",code.data[loop]);
			}
			printf("\n");
			break;
*/
		}
		printf("-- Preparing port... "); fflush(stdout);
		test = stc_core_prep(&stcc);
		if (test) {
			printf("** error %d (0x%08x)\n",test,CORESTAT(&stcc));
			break;
		}
		printf("done.\n");
#ifdef MY1DEBUG0
		show_core_baud(&stcc);
#endif
		printf("-- Sync device... "); fflush(stdout);
		test = stc_core_sync(&stcc);
		if (test==STC_SYNC_NONE) {
			printf("abort!\n");
			break;
		}
		else if (test==STC_SYNC_FAIL) {
			printf("** error %d (0x%08x)\n",test,CORESTAT(&stcc));
			break;
		}
		printf("done.\n");
		if (stcc.pdev) {
			printf("   MCU: %s [0x%04X]\n",stcc.pdev->name,stcc.uuid);
			printf("   Flash  Size: %2d kB\n",stcc.pdev->fmsz);
			printf("   EEPROM Size: %2d kB\n",stcc.pdev->emsz);
			printf("   MCU Freq: %d Hz\n",stcc.fmcu);
			printf("   BSL vers: %s\n",stcc.bslv);
#ifdef MY1DEBUG0
			show_core_baud(&stcc);
#endif
			/**stcc.pack.flag |= STC_PACK_FLAG_DEBUG;*/
			if (stcc.do_handshake) {
				printf("-- Handshake @%d... ",stcc.baud); fflush(stdout);
				test = stcc.do_handshake(&stcc);
				if (test) {
					printf("** error %d (0x%08x)\n",test,CORESTAT(&stcc));
/*
					printf("** error %d (0x%08x)[%d]\n",
						test,CORESTAT(&stcc),stcc.pack.step);
*/
					break;
				}
				printf("done.\n");
			}
			if (!stcc.code.fill) break;
			if (stcc.do_erase_mem) {
				printf("-- Erase device... "); fflush(stdout);
				test = stcc.do_erase_mem(&stcc);
				if (test) {
					printf("** error %d (0x%08x)\n",test,CORESTAT(&stcc));
					break;
				}
				printf("done. (%d/%d)\n",stcc.emx0,stcc.emx1);
				if (stcc.xuid[0]==0xff) {
					printf("@@ xUID:0x");
					for (loop=1;loop<8;loop++)
						printf("%02X",stcc.xuid[loop]);
					printf("\n");
				}
			}
			if (CORESTAT(&stcc)) {
				printf("** STAT! [0x%08x]\n",CORESTAT(&stcc));
				break;
			}
#ifdef MY1DEBUG0
			show_core_baud(&stcc);
#endif
			if (stcc.do_flash_mem) {
				printf("-- Flash device... "); fflush(stdout);
				test = stcc.do_flash_mem(&stcc);
				if (test) {
					printf("** error %d (0x%08x)\n",test,CORESTAT(&stcc));
					break;
				}
				printf("done. (%d/%d)\n",stcc.emx0,stcc.emx1);
			}
#ifdef MY1DEBUG0
			if (stcc.opts.fill>0) {
				printf("@@ Opts:");
				for (loop=0;loop<stcc.opts.fill;loop++)
					printf("[%02X]",stcc.opts.data[loop]);
				printf("\n");
			}
#endif
			if (stcc.do_send_opts) {
				printf("-- Send options... "); fflush(stdout);
				test = stcc.do_send_opts(&stcc);
				if (test) {
					printf("** error %d (0x%08x)[%d]\n",
						test,CORESTAT(&stcc),stcc.pack.step);
					break;
				}
				printf("done.\n");
				if (stcc.xuid[0]==0xfe) {
					printf("@@ xUID:0x");
					for (loop=1;loop<8;loop++)
						printf("%02X",stcc.xuid[loop]);
					printf("\n");
				}
			}
		}
		else show_packet(&stcc);
		if (CORESTAT(&stcc)) {
			printf("** STAT! [0x%08x]\n",CORESTAT(&stcc));
			break;
		}
	} while (0);
	if (stcc.do_reset_dev) {
		printf("-- Reset device... "); fflush(stdout);
		stcc.do_reset_dev(&stcc);
		printf("done.\n");
	}
#ifdef MY1DEBUG0
	show_core_baud(&stcc);
#endif
	printf("-- Core free... "); fflush(stdout);
	stc_core_free(&stcc);
	printf("done.\n\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
