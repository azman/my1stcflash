/*----------------------------------------------------------------------------*/
#include "my1stc_core.h"
#include "my1stc_stc12.h"
#include "my1stc_stc15w.h"
/*----------------------------------------------------------------------------*/
void stc_core_init(stc_core_t* core) {
	stc_pack_init(&core->pack,&core->port);
	bytes_init(&core->code);
	bytes_init(&core->opts);
	uart_init(&core->port);
	core->baud = 115200;
	core->hand = 2400;
	core->uuid = 0;
	core->fmcu = 0;
	core->xcnt = 0;
	core->bslv[0] = 0x0;
	core->pdev = 0x0;
	core->do_handshake = 0x0;
	core->do_erase_mem = 0x0;
	core->do_flash_mem = 0x0;
	core->do_send_opts = 0x0;
	core->do_reset_dev = 0x0;
}
/*----------------------------------------------------------------------------*/
void stc_core_free(stc_core_t* core) {
	uart_done(&core->port);
	bytes_free(&core->opts);
	bytes_free(&core->code);
	stc_pack_free(&core->pack);
}
/*----------------------------------------------------------------------------*/
int stc_core_prep(stc_core_t* core) {
	int pick;
	/* for now, simply open first port */
	pick = uart_find(&core->port,0x0);
	if (pick<=0) {
		CORESTAT(core) |= STC_CORE_FIND_ERROR;
		return CORESTAT(core);
	}
	if (!uart_prep(&core->port,pick)) {
		CORESTAT(core) |= STC_CORE_PORT_ERROR;
		return CORESTAT(core);
	}
	/** stc12 uses this! */
	uart_set_parity(&core->port,MY1PARITY_EVEN);
	/* handshake baudrate */
	pick = uart_set_baudrate(&core->port,core->hand);
	if (pick<0) pick = -pick; /* in case not changed! */
	if (pick!=core->hand) {
		CORESTAT(core) |= STC_CORE_BAUD_ERROR;
		return CORESTAT(core);
	}
	/* open comm line! */
	if (!uart_open(&core->port)) {
		CORESTAT(core) |= STC_CORE_OPEN_ERROR;
		return CORESTAT(core);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc_core_sync(stc_core_t* core) {
	int test;
	test = stc_pack_sync(&core->pack);
	if (test==STC_SYNC_DONE) {
		/* validate status packet size (payload size) */
		if (core->pack.rbuf.fill<STC_PACK_STAT_SIZE) {
			test = STC_SYNC_FAIL;
		}
		else {
			core->pdat = core->pack.rbuf.data;
			core->dcnt = core->pack.rbuf.fill;
			/* how to set read timeout? 15s? */
			/* byte read timeout 1s */
			core->pack.wait = 1000000;
			core->uuid = (int)core->pdat[20]<<8|core->pdat[21];
			core->pdev = stc_db_find(core->uuid);
			if (core->pdev) {
				if (core->pdev->flag&STCMCU_FLAG_STC12) {
					stc12_initstuff(core);
					if (CORESTAT(core)&STC_CORE_ERROR) {
						test = STC_SYNC_FAIL;
					}
					else {
						core->do_handshake = stc12_handshake;
						core->do_erase_mem = stc12_erasemem;
						core->do_flash_mem = stc12_flashmem;
						core->do_send_opts = stc12_sendopts;
						core->do_reset_dev = stc12_resetdev;
					}
				}
				else if (core->pdev->flag&STCMCU_FLAG_STC15) {
					stc15_initstuff(core);
					if (CORESTAT(core)&STC_CORE_ERROR) {
						test = STC_SYNC_FAIL;
					}
					else {
						core->do_handshake = stc15_handshake;
						core->do_erase_mem = stc15_erasemem;
						core->do_flash_mem = stc15_flashmem;
						core->do_send_opts = stc15_sendopts;
						core->do_reset_dev = stc12_resetdev;
					}
				}
			}
		}
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int stc_core_baud(stc_core_t* core, int baudrate) {
	int baudcode;
	my1uart_conf_t conf;
	uart_get_config(&core->port,&conf);
	baudcode = uart_encoded_baudrate(baudrate);
	if (baudcode!=conf.baud) {
		uart_done(&core->port);
		conf.baud = baudcode;
		uart_set_config(&core->port,&conf);
		uart_open(&core->port);
		uart_purge(&core->port); /* just in case */
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
