/*----------------------------------------------------------------------------*/
#include "my1linehex.h"
#include "my1text.h"
/*----------------------------------------------------------------------------*/
char is_hexchar(char ahex) {
	if (!(ahex>=0x30&&ahex<=0x39)&&!(ahex>=0x41&&ahex<=0x46)&&
			!(ahex>=0x61&&ahex<=0x66)) ahex = 0x0;
	return ahex;
}
/*----------------------------------------------------------------------------*/
int hex2nibb(signed char ahex) {
	if (ahex>=0x30&&ahex<=0x39) ahex -= 0x30;
	else if (ahex>=0x41&&ahex<=0x46) ahex -= 0x37;
	else if (ahex>=0x61&&ahex<=0x66) ahex -= 0x57;
	else ahex = 0xff; /* should not happen! */
	return ahex;
}
/*----------------------------------------------------------------------------*/
int hex2byte(char* phex) {
	int bval;
	bval = (hex2nibb(phex[0])<<4) + hex2nibb(phex[1]);
	return bval&0xFF;
}
/*----------------------------------------------------------------------------*/
int linehex_read(my1linehex_t* line, char* hstr) {
	int icnt, loop, csum, test, temp;
	icnt = 0; loop = 0;
	/* find icnt, filter newline! */
	while (hstr[icnt]) {
		if (icnt&&!is_hexchar(hstr[icnt]))
			return HEX_ERROR_CHAR;
		line->hstr[icnt] = hstr[icnt];
		if (hstr[icnt]=='\n'||hstr[icnt]=='\r') {
			line->hstr[icnt] = 0x0;
			break;
		}
		icnt++;
		if (icnt>COUNT_HEXSTR_CHAR)
			return HEX_ERROR_LENGTH;
	}
	/* check valid icnt - at least 11 characters */
	if (icnt<11) return HEX_ERROR_LENGTH;
	/* check first char */
	if (hstr[loop++]!=':') return HEX_ERROR_NOCOLON;
	/* get data count - usually max at 32 */
	line->size = hex2byte(&hstr[loop]);
	if (line->size>MAX_HEX_DATA_BYTE)
		return HEX_ERROR_SIZE;
	csum = line->size&0x0FF;
	loop += 2;
	/* get addr - highbyte */
	test = hex2byte(&hstr[loop]);
	csum += test&0xFF;
	loop += 2;
	line->addr = (test&0xFF)<<8;
	/* get addr - lowbyte */
	test = hex2byte(&hstr[loop]);
	csum += test&0xFF;
	loop += 2;
	line->addr |= (test&0xFF);
	/* get record type for csum calc */
	line->type = hex2byte(&hstr[loop]);
	csum += line->type&0xFF;
	loop += 2;
	/* get line type */
	if (line->type!=0x00&&line->type!=0x01)
		return HEX_ERROR_NOTDATA;
	/* calc range */
	line->next = line->addr + line->size;
	/* get data */
	for (temp=0;temp<line->size;temp++) {
		if (loop>=(icnt-2))
			return HEX_ERROR_LENGTH;
		test = hex2byte(&hstr[loop]);
		line->data[temp] = test&0xFF;
		csum += test&0xFF;
		loop += 2;
	}
	/* get csum */
	if (loop!=(icnt-2))
		return HEX_ERROR_LENGTH;
	test = hex2byte(&hstr[loop]);
	/* calculate and verify csum */
	csum = ~csum + 1;
	if ((test&0xFF)!=(csum&0xFF))
		return HEX_ERROR_CHECKSUM;
	/* returns record type */
	return line->type;
}
/*----------------------------------------------------------------------------*/
int linehex_2bin(my1linehex_t* line, my1bytes_t* pbin) {
	int init, loop;
	init = 0; /* line data counter */
/**
	printf("\n[DEBUG] {%s}",line->hstr);
	printf("\n[CHECK] <%02X:%02X> [%04x] :",line->type,line->size,line->addr);
	for (loop=0;loop<line->size;loop++)
		printf(" %02x",line->data[loop]);
*/
	/* zero padd to current address if needed */
	if (line->addr>pbin->size) {
		bytes_make(pbin,line->addr);
		if (pbin->size!=line->addr)
			return HEX_ERROR_MEMBIN;
		bytes_fill(pbin,0x0);
	}
	else if (line->addr<pbin->size) {
		//printf("\n[DEBUG1] {%04X|%04X}",line->addr,pbin->size);
		/* existing storage */
		for (loop=line->addr;loop<line->next&&loop<pbin->size;loop++) {
			pbin->data[loop] = line->data[init++];
			//printf(" [%02x]",pbin->data[loop]);
		}
	}
	if (line->next>MAX_HEX_CODE_SIZE)
		return HEX_ERROR_BINSIZE;
	/* storage to allocate */
	if (line->next>pbin->size) {
		bytes_make(pbin,line->next);
		if (pbin->size!=line->next)
			return HEX_ERROR_MEMBIN;
		/* will fill with data! */
	}
	//printf("\n[DEBUG2] {%04X|%04X}",init,pbin->fill);
	while (init<line->size) {
		bytes_slip(pbin,line->data[init++]);
		//printf(" [%02x]",pbin->data[pbin->fill-1]);
	}
	if (pbin->fill!=pbin->size)
		return HEX_ERROR_MEMBIN;
	return line->size;
}
/*----------------------------------------------------------------------------*/
int bytes_load_ihex(my1bytes_t* pbin,char* phex) {
	my1linehex_t line;
	my1text_t text;
	int test, temp;
	test = 0;
	text_init(&text);
	text_open(&text,phex);
	if (text.pfile) {
		while (text_read(&text)>=CHAR_INIT) {
			temp = linehex_read(&line,text.pbuff.buff);
			if (temp<0) {
				test = temp; /* get errorcode */
				break;
			}
			if (temp) {
				test = pbin->size;
				break; /* type 0x01 end of file found! */
			}
			/* it's data! */
			temp = linehex_2bin(&line,pbin);
			if (temp<0) {
				test = temp;
				break;
			}
		}
		text_done(&text);
	}
	else test = HEX_ERROR_FILE;
	text_free(&text);
	return test;
}
/*----------------------------------------------------------------------------*/
