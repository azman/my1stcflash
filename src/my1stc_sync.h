/*----------------------------------------------------------------------------*/
#ifndef __MY1STC_SYNC_H__
#define __MY1STC_SYNC_H__
/*----------------------------------------------------------------------------*/
#include "my1uart.h"
/*----------------------------------------------------------------------------*/
#define STC_SYNC_CHAR 0x7f
#define STC_SYNC_WAIT_US 30000
#define STC_SYNC_INIT -2
#define STC_SYNC_MISS -1
#define STC_SYNC_DONE 0
#define STC_SYNC_NONE 1
/*----------------------------------------------------------------------------*/
#define STC_SYNC_BAUD 2400
/*----------------------------------------------------------------------------*/
typedef struct _stc_sync_t {
	unsigned int stat; /* status bits */
	int wait; /* wait time in microsecs */
	my1uart_t *port;
} stc_sync_t;
/*----------------------------------------------------------------------------*/
#define __NO_USLEEP__
/* just in case we dont want usleep */
#ifdef __NO_USLEEP__
void stc_wait_us(int);
#else
#include <unistd.h>
#define stc_wait_us(chk) usleep(chk)
#endif
/*----------------------------------------------------------------------------*/
int stc_sync_exec(stc_sync_t*,int,int);
void stc_sync_init(stc_sync_t*,my1uart_t*);
/*----------------------------------------------------------------------------*/
#endif /** __MY1STC_SYNC_H__ */
/*----------------------------------------------------------------------------*/
