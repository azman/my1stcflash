/*----------------------------------------------------------------------------*/
#include "my1stc_db.h"
/*----------------------------------------------------------------------------*/
const stcmcu_t stcmcu_db[] = {
	{ 0xd17e,STCMCU_STC12C5A60S2,60,1,"STC12C5A60S2" },
	{ 0xf514,STCMCU_STC15W204S,4,1,"STC15W204S" }
};
/*----------------------------------------------------------------------------*/
const int STCMCU_DBSIZE = sizeof(stcmcu_db)/sizeof(stcmcu_t);
/*----------------------------------------------------------------------------*/
stcmcu_t* stc_db_find(int uuid) {
	int loop;
	stcmcu_t* find;
	for (loop=0,find=0x0;loop<STCMCU_DBSIZE;loop++) {
		if (stcmcu_db[loop].uuid==uuid) {
			find = (stcmcu_t*) &stcmcu_db[loop];
			break;
		}
	}
	return find;
}
/*----------------------------------------------------------------------------*/
