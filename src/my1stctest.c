/*----------------------------------------------------------------------------*/
#include "my1stc_pack.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define PROGNAME "my1stcsync"
#ifndef PROGVERS
#define PROGVERS "build"
#endif
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	stc_pack_t pack;
	my1uart_t port;
	int baud, pick;
	printf("\n%s - STC Sync Test (version %s)\n",PROGNAME,PROGVERS);
	printf("  => by azman@my1matrix.org\n\n");
	uart_init(&port);
	stc_pack_init(&pack,&port);
	baud = 2400;
	if (argc>1) {
		if (!strncmp(argv[1],"--debug",8)) {
			pack.flag |= STC_PACK_FLAG_DEBUG;
		}
	}
	do {
		printf("-- Preparing port... "); fflush(stdout);
		/* for now, simply open first port */
		pick = uart_find(&port,0x0);
		if (pick<=0) {
			printf("** error finding!\n");
			break;
		}
		if (!uart_prep(&port,pick)) {
			printf("** error preparing!\n");
			break;
		}
		/** stc12 uses this! */
		uart_set_parity(&port,MY1PARITY_EVEN);
		/* handshake baudrate */
		pick = uart_set_baudrate(&port,baud);
		if (pick<0) pick = -pick; /* in case not changed! */
		if (pick!=baud) {
			printf("** error setting baud!\n");
			break;
		}
		/* open comm line! */
		if (!uart_open(&port)) {
			printf("** error opening!\n");
			break;
		}
		uart_purge(&port);
		printf("done.\n");
		printf("-- Sync device... "); fflush(stdout);
		if (stc_pack_sync(&pack)==STC_SYNC_NONE) {
			printf("** abort!\n");
			break;
		}
		printf("done.\n");
	} while (0);
	uart_done(&port);
	stc_pack_free(&pack);
	putchar('\n');
	return 0;
}
/*----------------------------------------------------------------------------*/
