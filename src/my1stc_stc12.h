/*----------------------------------------------------------------------------*/
#ifndef __MY1STC_STC12_H__
#define __MY1STC_STC12_H__
/*----------------------------------------------------------------------------*/
#include "my1stc_core.h"
#include <math.h>
/*----------------------------------------------------------------------------*/
#define STC12_PACK_MINSIZE 29
#define STC12_CODE_BLOCKSIZE 128
#define STC12_ERASE_COUNTDOWN 0x0d
/*----------------------------------------------------------------------------*/
#define STC12_INITBAUD_ERROR   (STC_CORE_ERROR|0x0100)
#define STC12_HANDSHAKE1_ERROR (STC_CORE_ERROR|0x0200)
#define STC12_HANDSHAKE2_ERROR (STC_CORE_ERROR|0x0300)
#define STC12_HANDSHAKE3_ERROR (STC_CORE_ERROR|0x0400)
#define STC12_ERASEMEM_ERROR   (STC_CORE_ERROR|0x0500)
#define STC12_FLASHMEM1_ERROR  (STC_CORE_ERROR|0x0600)
#define STC12_FLASHMEM2_ERROR  (STC_CORE_ERROR|0x0700)
/*----------------------------------------------------------------------------*/
int stc12_initstuff(stc_core_t* core) {
	int loop, fcnt, temp;
	float calc;
	/* detect target frequency */
	for (loop=0,fcnt=0;loop<8;loop++) {
		temp = (int)core->pdat[(loop<<1)+1]<<8|core->pdat[(loop<<1)+2];
		fcnt += temp;
	}
	calc = (float)fcnt/8.0;
	calc = calc * core->hand * 12.0 / 7.0;
	core->fmcu = (int)calc;
	/* get bsl firmware version */
	core->bslv[0] = (core->pdat[17]>>4)+0x30;
	core->bslv[1] = '.';
	core->bslv[2] = (core->pdat[17]&0x0f)+0x30;
	core->bslv[3] = core->pdat[18];
	core->bslv[4] = 0x0;
	/* get options */
	if (core->dcnt<STC12_PACK_MINSIZE)
		CORESTAT(core) |= STC_CORE_STAT_ERROR;
	else {
		/* should have 4 bytes msr[0]-msr[3] */
		bytes_more(&core->opts,&core->pdat[23],3);
		bytes_slip(&core->opts,core->pdat[27]);
	}
/**
"reset_pin_enabled", self.get_reset_pin_enabled, self.set_reset_pin_enabled),
"low_voltage_reset", self.get_low_voltage_detect, self.set_low_voltage_detect),
"oscillator_stable_delay", self.get_osc_stable_delay, self.set_osc_stable_delay),
"por_reset_delay", self.get_por_delay, self.set_por_delay),
"clock_gain", self.get_clock_gain, self.set_clock_gain),
"clock_source", self.get_clock_source, self.set_clock_source),
"watchdog_por_enabled", self.get_watchdog, self.set_watchdog),
"watchdog_stop_idle", self.get_watchdog_idle, self.set_watchdog_idle),
"watchdog_prescale", self.get_watchdog_prescale, self.set_watchdog_prescale),
"eeprom_erase_enabled", self.get_ee_erase, self.set_ee_erase),
"bsl_pindetect_enabled", self.get_pindetect, self.set_pindetect),
**/
	return 0;
}
/*----------------------------------------------------------------------------*/
typedef struct _stc12_shaked_t {
	int baud, bsum, errb, iapw, dlay;
} stc12_shaked_t;
/*----------------------------------------------------------------------------*/
int stc12_iapw_value(int freq) {
	int wait = 0x80;
	if (freq < 1000000) wait = 0x87;
	else if (freq < 2000000) wait = 0x86;
	else if (freq < 3000000) wait = 0x85;
	else if (freq < 6000000) wait = 0x84;
	else if (freq < 12000000) wait = 0x83;
	else if (freq < 20000000) wait = 0x82;
	else if (freq < 24000000) wait = 0x81;
	return wait;
}
/*----------------------------------------------------------------------------*/
int stc12_calc_baud(stc_core_t* core, stc12_shaked_t* pdat) {
	int test, calc;
	test = 256-(int)round((float)core->fmcu/(core->baud*16));
	/* check if baudrate achievable */
	if (test<=1||test>255) {
		CORESTAT(core) |= STC12_INITBAUD_ERROR;
		return -1;
	}
	pdat->baud = test;
	/* calc baud error */
	calc = (int)round((float)core->fmcu/(16*(256-test)));
	pdat->errb = core->baud-calc;
	if (pdat->errb<0) pdat->errb = -pdat->errb;
	pdat->errb = pdat->errb*100/core->baud;
	if (pdat->errb>5) {
		CORESTAT(core) |= STC12_INITBAUD_ERROR;
		return -2;
	}
	/* baud counter value */
	pdat->bsum = (2*(256-test))&0xff;
	/* iap wait states */
	pdat->iapw = stc12_iapw_value(core->fmcu);
	/* MCU delay after switching baud rates */
	pdat->dlay = 0xa0; /* stc-isp=>0xa0, stc-gal=>0x40 ? now 0x80 */
	return pdat->errb;
}
/*----------------------------------------------------------------------------*/
int stc12_handshake(stc_core_t* core) {
	byte08_t chk1[] = { 0x50, 0x00, 0x00, 0x36, 0x01 };
	byte08_t chk2[] = { 0x8f, 0xc0, 0xff, 0x3f, 0xff, 0xff, 0xff };
	stc12_shaked_t temp;
	int test;
	test = stc12_calc_baud(core,&temp);
	if (test<0) return CORESTAT(core);
	bytes_null(&core->pack.tbuf);
	bytes_more(&core->pack.tbuf,chk1,sizeof(chk1));
	bytes_slip(&core->pack.tbuf,((core->uuid&0xff00)>>8));
	bytes_slip(&core->pack.tbuf,(core->uuid&0xff));
	stc_pack_send(&core->pack);
	test = stc_pack_wait(&core->pack);
	if (test!=STC_PACK_OK||core->pdat[0]!=0x8f) {
		CORESTAT(core) |= STC12_HANDSHAKE1_ERROR;
		return CORESTAT(core);
	}
	chk2[2] = temp.baud;
	chk2[4] = temp.bsum;
	chk2[5] = temp.dlay;
	chk2[6] = temp.iapw;
	bytes_null(&core->pack.tbuf);
	bytes_more(&core->pack.tbuf,chk2,sizeof(chk2));
	stc_pack_send(&core->pack);
	stc_wait_us(100000);
	stc_core_baud(core,core->baud);
	test = stc_pack_wait(&core->pack);
	stc_core_baud(core,core->hand);
	if (test!=STC_PACK_OK||core->pdat[0]!=0x8f) {
		CORESTAT(core) |= STC12_HANDSHAKE2_ERROR;
		return CORESTAT(core);
	}
	chk2[0] = 0x8e;
	bytes_null(&core->pack.tbuf);
	bytes_more(&core->pack.tbuf,chk2,sizeof(chk2)-1); /* skip iapw */
	stc_pack_send(&core->pack);
	stc_wait_us(100000);
	stc_core_baud(core,core->baud);
	test = stc_pack_wait(&core->pack);
	if (test!=STC_PACK_OK||core->pdat[0]!=0x84) {
		CORESTAT(core) |= STC12_HANDSHAKE3_ERROR;
		return CORESTAT(core);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc12_erasemem(stc_core_t* core) {
	int blks, size, loop;
	blks = core->code.fill;
	blks = (int)((blks+511)/512)*2; /* blocks to erase */
	size = core->pdev->fmsz * 1024; /* fmsz in kB! */
	size = (int)((size+511)/512)*2; /* number of blocks */
	/* blks & size should be 512-byte aligned (physical size) */
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x84);
	bytes_slip(&core->pack.tbuf,0xff);
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,(blks&0xff));
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,0x00);
	bytes_slip(&core->pack.tbuf,(size&0xff));
	bytes_fill_size(&core->pack.tbuf,0x00,12);
	for (loop=0x80;loop>STC12_ERASE_COUNTDOWN;loop--)
		bytes_slip(&core->pack.tbuf,(loop&0xff));
	stc_pack_send(&core->pack);
	if (stc_pack_wait(&core->pack)!=STC_PACK_OK||core->pdat[0]!=0x00) {
		CORESTAT(core) |= STC12_ERASEMEM_ERROR;
		return CORESTAT(core);
	}
	core->emx0 = blks;
	core->emx1 = size;
	if (core->dcnt>=8) {
		core->xuid[0] = 0xff;
		for (loop=1;loop<8;loop++)
			core->xuid[loop] = core->pdat[loop];
	}
	else core->xuid[0] = 0x00;
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc12_flashmem(stc_core_t* core) {
	int size, curr, loop;
	byte08_t *pdat, chk1[] = { 0x00, 0x00, 0x00,
			0xff, 0xff, 0x00, STC12_CODE_BLOCKSIZE };
	size = STC12_CODE_BLOCKSIZE+7;
	if (core->pack.tbuf.size<size)
		bytes_make(&core->pack.tbuf,size);
	pdat = core->code.data; loop = core->code.fill;
	/* send in 'packets' */
	bytes_null(&core->pack.tbuf);
	bytes_more(&core->pack.tbuf,chk1,sizeof(chk1));
	for (curr=0;curr<core->code.fill;curr+=STC12_CODE_BLOCKSIZE) {
		core->pack.tbuf.data[3] = ((curr&0xff00)>>8)&0xff;
		core->pack.tbuf.data[4] = curr&0xff;
		core->pack.tbuf.fill = sizeof(chk1);
		/* need to make this faster! link instead of copy? */
		if (loop>=STC12_CODE_BLOCKSIZE) {
			bytes_more(&core->pack.tbuf,pdat,STC12_CODE_BLOCKSIZE);
			loop -= STC12_CODE_BLOCKSIZE;
		}
		else {
			/* this should not happened? aligned? */
			bytes_more(&core->pack.tbuf,pdat,loop);
			bytes_fill_size(&core->pack.tbuf,0x00,STC12_CODE_BLOCKSIZE-loop);
		}
		pdat += STC12_CODE_BLOCKSIZE;
		stc_pack_send(&core->pack);
		if (stc_pack_wait(&core->pack)!=STC_PACK_OK||core->pdat[0]!=0x00) {
			CORESTAT(core) |= STC12_FLASHMEM1_ERROR;
			return CORESTAT(core);
		}
	}
	chk1[0] = 0x69;
	chk1[3] = 0x36;
	chk1[4] = 0x01;
	chk1[5] = ((core->uuid&0xff00)>>8);
	chk1[6] = core->uuid&0xff;
	bytes_null(&core->pack.tbuf);
	bytes_more(&core->pack.tbuf,chk1,7);
	stc_pack_send(&core->pack);
	if (stc_pack_wait(&core->pack)!=STC_PACK_OK||core->pdat[0]!=0x8d) {
		CORESTAT(core) |= STC12_FLASHMEM2_ERROR;
		return CORESTAT(core);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc12_sendopts(stc_core_t* core) {
	int loop;
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x8d);
	bytes_more(&core->pack.tbuf,core->opts.data,4);
	bytes_fill_size(&core->pack.tbuf,0xff,4);
	bytes_slip(&core->pack.tbuf,core->opts.data[3]);
	bytes_fill_size(&core->pack.tbuf,0xff,7);
	bytes_slip(&core->pack.tbuf,(byte08_t)((core->fmcu>>24)&0xff));
	bytes_slip(&core->pack.tbuf,(byte08_t)((core->fmcu>>16)&0xff));
	bytes_slip(&core->pack.tbuf,(byte08_t)((core->fmcu>>8)&0xff));
	bytes_slip(&core->pack.tbuf,(byte08_t)(core->fmcu&0xff));
	stc_pack_send(&core->pack);
	if (stc_pack_wait(&core->pack)!=STC_PACK_OK||core->pdat[0]!=0x50) {
		CORESTAT(core) |= STC12_ERASEMEM_ERROR;
		return CORESTAT(core);
	}
	if (!core->xuid[0]&&core->dcnt>=25) {
		core->xuid[0] = 0xfe;
		for (loop=1;loop<8;loop++)
			core->xuid[loop] = core->pdat[loop+17];
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int stc12_resetdev(stc_core_t* core) {
	bytes_null(&core->pack.tbuf);
	bytes_slip(&core->pack.tbuf,0x82);
	stc_pack_send(&core->pack);
	/* do not wait for reply! */
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1STC_STC12_H__ */
/*----------------------------------------------------------------------------*/
