/*----------------------------------------------------------------------------*/
#ifndef __MY1LINEHEX_H__
#define __MY1LINEHEX_H__
/*----------------------------------------------------------------------------*/
/**
 * my1linehex.h - textline reader for intel-hex format
**/
/*----------------------------------------------------------------------------*/
#include "my1bytes.h"
/*----------------------------------------------------------------------------*/
#define MAX_HEX_CODE_SIZE 0x10000
#define MAX_HEX_DATA_BYTE 32
#define COUNT_HEXSTR_BYTE (1+2+1+MAX_HEX_DATA_BYTE+1)
#define COUNT_HEXSTR_CHAR (1+COUNT_HEXSTR_BYTE*2+1)
#define COUNT_HEXSTR_BUFF (COUNT_HEXSTR_CHAR+2)
/*----------------------------------------------------------------------------*/
#define HEX_ERROR_GENERAL -1
#define HEX_ERROR_FILE -2
#define HEX_ERROR_LENGTH -3
#define HEX_ERROR_NOCOLON -4
#define HEX_ERROR_NOTDATA -5
#define HEX_ERROR_CHECKSUM -6
#define HEX_ERROR_OVERFLOW -7
#define HEX_ERROR_ACK -8
#define HEX_ERROR_INVALID -9
#define HEX_ERROR_SIZE -10
#define HEX_ERROR_BINSIZE -11
#define HEX_ERROR_MEMBIN -12
#define HEX_ERROR_CHAR -13
#define HEX_ERROR_OVERLAPPED -14
/*----------------------------------------------------------------------------*/
typedef struct _my1linehex_t {
	unsigned int size, addr, type, next;
	unsigned int data[MAX_HEX_DATA_BYTE];
	char hstr[COUNT_HEXSTR_BUFF];
} my1linehex_t;
/*----------------------------------------------------------------------------*/
int linehex_read(my1linehex_t* line, char* hstr);
int linehex_2bin(my1linehex_t* line, my1bytes_t* pbin);
int bytes_load_ihex(my1bytes_t* pbin,char* phex);
/*----------------------------------------------------------------------------*/
#endif /** __MY1LINEHEX_H__ */
/*----------------------------------------------------------------------------*/
