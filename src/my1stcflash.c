/*----------------------------------------------------------------------------*/
#include "my1stc_core.h"
#include "my1linehex.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define PROGNAME "my1stcflash"
#ifndef PROGVERS
#define PROGVERS "build"
#endif
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	stc_core_t stcc;
	char *pcod, *pchk;
	int test,loop;
	printf("\n%s - STC Flash Tool (version %s)\n",PROGNAME,PROGVERS);
	printf("  => by azman@my1matrix.org\n\n");
	stc_core_init(&stcc);
	pcod = 0x0;
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]=='-') {
			if (argv[loop][1]=='-') test = 2; else test = 1;
			pchk = &argv[loop][test];
		}
		else {
			/* check if this is a readable file */
			FILE* test;
			test = fopen(argv[loop],"r");
			if (test) {
				fclose(test);
				pcod = argv[loop];
			}
			else {
				printf("** Unknown param '%s'!\n",argv[loop]);
				CORESTAT(&stcc) |= STC_CORE_ERROR;
			}
			continue;
		}
		if (!strncmp(pchk,"baud",5)) {
			if (get_param_int(argc,argv,&loop,&test)<0) {
				printf("** Cannot get transfer baud rate!\n");
				CORESTAT(&stcc) |= STC_CORE_ERROR;
			}
			else if (uart_encoded_baudrate(test)<0) {
				printf("** Invalid transfer baud rate! (%d)\n",test);
				CORESTAT(&stcc) |= STC_CORE_ERROR;
			}
			else stcc.baud = test;
		}
		else if (!strncmp(pchk,"hand",5)) {
			if (get_param_int(argc,argv,&loop,&test)<0) {
				printf("** Cannot get handshake baud rate!\n");
				CORESTAT(&stcc) |= STC_CORE_ERROR;
			}
			else if (uart_encoded_baudrate(test)<0) {
				printf("** Invalid handshake baud rate! (%d)\n",test);
				CORESTAT(&stcc) |= STC_CORE_ERROR;
			}
			else stcc.hand = test;
		}
		else {
			printf("** Invalid option '%s'!\n",argv[loop]);
			CORESTAT(&stcc) |= STC_CORE_ERROR;
		}
	}
	do {
		if (CORESTAT(&stcc)&STC_CORE_ERROR) break;
		if (pcod) {
			printf("-- Loading code HEX file... ");
			test = bytes_load_ihex(&stcc.code, pcod);
			if (test<0) {
				printf("fail! (%d)\n",test);
				break;
			}
			printf("done! (%d/%d)\n",stcc.code.fill,stcc.code.size);
			/* align to block size? */
			test = stcc.code.size%512;
			if (test) {
				bytes_make(&stcc.code,stcc.code.size+(512-test));
				bytes_fill(&stcc.code,0xff);
			}
		}
		printf("-- Preparing port... "); fflush(stdout);
		test = stc_core_prep(&stcc);
		if (test) {
			printf("** error (0x%08x)\n",CORESTAT(&stcc));
			break;
		}
		printf("done.\n");
		printf("-- Sync device @%d... ",stcc.hand); fflush(stdout);
		test = stc_core_sync(&stcc);
		if (test) {
			printf("** error (0x%08x)\n",CORESTAT(&stcc));
			break;
		}
		printf("done.\n");
		if (stcc.pdev) {
			printf("   MCU: %s [0x%04X]\n",stcc.pdev->name,stcc.uuid);
			printf("   Flash  Size: %2d kB\n",stcc.pdev->fmsz);
			printf("   EEPROM Size: %2d kB\n",stcc.pdev->emsz);
			printf("   MCU Freq: %d Hz\n",stcc.fmcu);
			printf("   BSL vers: %s\n",stcc.bslv);
			if (stcc.do_handshake) {
				printf("-- Handshake @%d... ",stcc.baud); fflush(stdout);
				test = stcc.do_handshake(&stcc);
				if (test) {
					printf("** error (0x%08x)\n",CORESTAT(&stcc));
					break;
				}
				printf("done.\n");
			}
			if (!stcc.code.fill) break;
			if (stcc.do_erase_mem) {
				printf("-- Erase device... "); fflush(stdout);
				test = stcc.do_erase_mem(&stcc);
				if (test) {
					printf("** error (0x%08x)\n",CORESTAT(&stcc));
					break;
				}
				printf("done. (%d/%d)\n",stcc.emx0,stcc.emx1);
				if (stcc.xuid[0]==0xff) {
					printf("@@ xUID:0x");
					for (loop=1;loop<8;loop++)
						printf("%02X",stcc.xuid[loop]);
					printf("\n");
				}
			}
			if (stcc.do_flash_mem) {
				printf("-- Flash device... "); fflush(stdout);
				test = stcc.do_flash_mem(&stcc);
				if (test) {
					printf("** error (0x%08x)\n",CORESTAT(&stcc));
					break;
				}
				printf("done. (%d/%d)\n",stcc.emx0,stcc.emx1);
			}
			if (stcc.do_send_opts) {
				printf("-- Send options... "); fflush(stdout);
				test = stcc.do_send_opts(&stcc);
				if (test) {
					printf("** error (0x%08x)\n",CORESTAT(&stcc));
					break;
				}
				printf("done.\n");
				if (stcc.xuid[0]==0xfe) {
					printf("@@ xUID:0x");
					for (loop=1;loop<8;loop++)
						printf("%02X",stcc.xuid[loop]);
					printf("\n");
				}
			}
		}
	} while (0);
	if (stcc.do_reset_dev) {
		printf("-- Reset device... "); fflush(stdout);
		stcc.do_reset_dev(&stcc);
		printf("done.\n");
	}
	stc_core_free(&stcc);
	putchar('\n');
	return 0;
}
/*----------------------------------------------------------------------------*/
